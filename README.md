[Front-end Template](http://adminlte.io "Admin LTE.io")


## Features:

- [x] Vote preview
- [x] Multiple votes
- [x] Result tally via Horizontal Bar Chart
- [x] Print voting results in PDF
- [x] Changeable order of positions to show in the ballot
- [x] CRUD voters
- [x] CRUD candidates
- [x] CRUD positions
- [x] Plugins
- [x] AdminLTE Template

### A. Admin Users Can
1. See Overall Summary Charts of Votes
2. Reset Votes
4. Manage Voters (CRUD)
5. Manage Candidates (CRUD)
6. Manage Positions (CRUD)
7. Change Ballot Style (Ballot Position)
8. Update/Change Ballot Title

### B. Voters Can
1. Register
2. Login
3. Verify with OTP (This can be overwritten in `settings.py` file)
4. Votes for their favourite candidates
5. View candidates they voted for

## 📸 ScreenShots
<figure>
  <img
  src="ss/DB.png"
  alt="The beautiful MDN logo.">
  <figcaption>Database Design</figcaption>
</figure>

**Default Credentials**

*For HOD /SuperAdmin*
Email: admin@admin.com
Password: admin

*For Staff*
Email: staff@staff.com
Password: staff



## How the system works
Administrator is required to have created candidates. 
Before creating candidates, the admin must have created positions
After doing this, the voters can vote (provided that they are registered and verified)

## How do voters get verified ?
OTP is sent to voter's phone. In a case of OTP delivery error, voter can request for OTP again. 
The OTP is sent via an SMS gateway. 
Voters can request for OTP for a maximum of three times.
Same OTP is sent to voters

## Can OTP verification be bypassed ?
Yeah, sure.
Open `settings.py` and toggle `SEND_OTP` to  `False`
Then, wait till server restarts


