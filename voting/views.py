from django.shortcuts import render, redirect, reverse
from account.views import account_login
from .models import *
from django.http import JsonResponse
from django.utils.text import slugify
from django.contrib import messages
from django.conf import settings
from django.http import JsonResponse
import requests
import json
# Create your views here.

import cv2
import os
import time
from .face_verify import SimpleFacerec
import hashlib
from PIL import Image
import os
import numpy as np

def index(request):
    if not request.user.is_authenticated:
        return account_login(request)
    context = {}
    # return render(request, "voting/login.html", context)


def generate_ballot(display_controls=False):
    positions = Ballot.objects.order_by('priority').all()
    output = ""
    num = 1
    # print (positions[0].position_name)
    for position in positions:
        name = position.name
        position_name = slugify(name)
        variants = BallotVariant.objects.filter(ballot=position)
        
        instruction = ""
        candidates_data = ""
        
        for variant in variants:
            if position.max_vote > 1:
                instruction = f"You may select up to {position.max_vote} candidates"
                input_box = f'<input type="checkbox" value="{variant.id}" class="flat-red {position_name}" name="{position_name}[]">'
            else:
                instruction = "Select only one candidate"
                input_box = f'<input value="{variant.id}" type="radio" class="flat-red {position_name}" name="{position_name}">'
            
            # Handling Candidate variant
            if variant.content_type.model == "candidate":
                image = f"/media/{variant.content_object.image.url}"  # assuming image field is used
                candidates_data += f'''<li>{input_box}
                <button type="button" class="btn btn-primary btn-sm btn-flat clist platform" 
                data-fullname="{variant.content_object.name}" 
                data-bio="{variant.content_object.bio}">
                <i class="fa fa-search"></i> Platform</button>
                <img src="{image}" height="100px" width="100px" class="clist">
                <span class="cname clist">{variant.content_object.name}</span></li>'''
            
            # Handling TextField variant
            elif variant.content_type.model == "textfieldoption":
                candidates_data += f'''<li>{input_box}
                <span class="cname clist">{variant.content_object.content}</span></li>'''
        
        up = 'disabled' if position.priority == 1 else ''
        down = 'disabled' if position.priority == positions.count() else ''
        
        output += f"""<div class="row">
        <div class="col-xs-12">
        <div class="box box-solid" id="{position.id}">
        <div class="box-header with-border">
        <h3 class="box-title"><b>{name}</b></h3>"""

        if display_controls:
            output += f"""<div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-sm moveup" data-id="{position.id}" {up}>
            <i class="fa fa-arrow-up"></i> </button>
            <button type="button" class="btn btn-default btn-sm movedown" data-id="{position.id}" {down}>
            <i class="fa fa-arrow-down"></i></button>
            </div>"""

        output += f"""</div>
        <div class="box-body">
        <p>{instruction}
        <span class="pull-right">
        <button type="button" class="btn btn-success btn-sm btn-flat reset" data-desc="{position_name}">
        <i class="fa fa-refresh"></i> Reset</button>
        </span>
        </p>
        <div id="candidate_list">
        <ul>
        {candidates_data}
        </ul>
        </div>
        </div>
        </div>
        </div>
        </div>"""

        position.priority = num
        position.save()
        num += 1

    return output

# def generate_ballot(display_controls=False):
#     positions = Ballot.objects.order_by('priority').all()
#     output = ""
#     num = 1

#     for position in positions:
#         name = position.name
#         position_name = slugify(name)
#         variants = BallotVariant.objects.filter(ballot=position)
        
#         instruction = ""
#         candidates_data = ""
        
#         for variant in variants:
#             if position.max_vote > 1:
#                 instruction = f"You may select up to {position.max_vote} candidates"
#                 input_box = f'<input type="checkbox" value="{variant.id}" class="flat-red {position_name}" name="{position_name}[]">'
#             else:
#                 instruction = "Select only one candidate"
#                 input_box = f'<input value="{variant.id}" type="radio" class="flat-red {position_name}" name="{position_name}">'
            
#             if variant.content_type.model == "candidate":
#                 image = f"/media/{variant.content_object.photo}"
#                 candidates_data += f'''<li>{input_box}
#                 <button type="button" class="btn btn-primary btn-sm btn-flat clist platform" 
#                 data-fullname="{variant.content_object.name}" 
#                 data-bio="{variant.content_object.bio}">
#                 <i class="fa fa-search"></i> Platform</button>
#                 <img src="{image}" height="100px" width="100px" class="clist">
#                 <span class="cname clist">{variant.content_object.name}</span></li>'''
#             else:
#                 candidates_data += f'''<li>{input_box}
#                 <span class="cname clist">{variant.content_object.name}</span></li>'''
        
#         up = 'disabled' if position.priority == 1 else ''
#         down = 'disabled' if position.priority == positions.count() else ''
        
#         output += f"""<div class="row">
#         <div class="col-xs-12">
#         <div class="box box-solid" id="{position.id}">
#         <div class="box-header with-border">
#         <h3 class="box-title"><b>{name}</b></h3>"""

#         if display_controls:
#             output += f"""<div class="pull-right box-tools">
#             <button type="button" class="btn btn-default btn-sm moveup" data-id="{position.id}" {up}>
#             <i class="fa fa-arrow-up"></i> </button>
#             <button type="button" class="btn btn-default btn-sm movedown" data-id="{position.id}" {down}>
#             <i class="fa fa-arrow-down"></i></button>
#             </div>"""

#         output += f"""</div>
#         <div class="box-body">
#         <p>{instruction}
#         <span class="pull-right">
#         <button type="button" class="btn btn-success btn-sm btn-flat reset" data-desc="{position_name}">
#         <i class="fa fa-refresh"></i> Reset</button>
#         </span>
#         </p>
#         <div id="candidate_list">
#         <ul>
#         {candidates_data}
#         </ul>
#         </div>
#         </div>
#         </div>
#         </div>
#         </div>"""

#         position.priority = num
#         position.save()
#         num += 1

#     return output

def fetch_ballot(request):
    output = generate_ballot(display_controls=True)
    return JsonResponse(output, safe=False)


def generate_otp():
    """Link to this function
    https://www.codespeedy.com/otp-generation-using-random-module-in-python/
    """
    import random as r
    otp = ""
    for i in range(r.randint(5, 8)):
        otp += str(r.randint(1, 9))

    print(otp)
    return otp



def dashboard(request):
    positions = Ballot.objects.all().order_by('priority')
    candidates = Candidate.objects.all()
    voters = Voter.objects.all()
    chart_data = {}
    ballot_form = ""
    
    # Generate the ballot form for voting
    for position in positions:
        name = position.name
        position_name = slugify(name)
        variants = BallotVariant.objects.all()

        variants = BallotVariant.objects.filter(ballot=position)
        instruction = ""
        candidates_data = ""

        for variant in variants:
            if position.max_vote > 1:
                instruction = f"You may select up to {position.max_vote} options"
                input_box = f'<input type="checkbox" value="{variant.id}" class="flat-red {position_name}" name="{position_name}[]">'
            else:
                instruction = "Select only one option"
                input_box = f'<input value="{variant.id}" type="radio" class="flat-red {position_name}" name="{position_name}">'

            # Display candidate or text field option
            if variant.content_type.model == "candidate":
                image = f"/media/{variant.content_object.image.url}"
                candidates_data += f'''<li>{input_box}
                <button type="button" class="btn btn-primary btn-sm btn-flat clist platform" 
                data-fullname="{variant.content_object.name}" 
                data-bio="{variant.content_object.bio}">
                <i class="fa fa-search"></i> Platform</button>
                <img src="{image}" height="100px" width="100px" class="clist">
                <span class="cname clist">{variant.content_object.name}</span></li>'''
            else:
                candidates_data += f'''<li>{input_box}
                <span class="cname clist">{variant.content_object.name}</span></li>'''

        # Generate the HTML structure for the voting form
        ballot_form += f"""<div class="row">
        <div class="col-xs-12">
        <div class="box box-solid" id="{position.id}">
        <div class="box-header with-border">
        <h3 class="box-title"><b>{name}</b></h3>
        </div>
        <div class="box-body">
        <p>{instruction}</p>
        <div id="candidate_list">
        <ul>
        {candidates_data}
        </ul>
        </div>
        </div>
        </div>
        </div>
        </div>"""
    
    print("12submit erro", request.method, request)

    # Handle vote submission
    # if request.method == 'POST':
    #     print("submit erro")
    #     for position in positions:
    #         position_name = slugify(position.name)
    #         if position.max_vote > 1:
    #             selected_variants = request.POST.getlist(f"{position_name}[]")
    #         else:
    #             selected_variants = [request.POST.get(f"{position_name}")]

    #         for variant_id in selected_variants:
    #             if variant_id:
    #                 variant = BallotVariant.objects.get(id=variant_id)
    #                 Votes.objects.create(candidate=variant, position=position, voter=request.user.voter)

    #     messages.success(request, "Your votes have been submitted successfully!")
    #     return redirect('dashboard')

    # Prepare the chart data for the dashboard
    for position in positions:
        list_of_variants = []
        votes_count = []

        for variant in BallotVariant.objects.filter(ballot=position):
            variant_name = variant.content_object.name
            list_of_variants.append(variant_name)
            votes = Votes.objects.filter(candidate=variant, position=position).count()
            votes_count.append(votes)

        chart_data[position.name] = {
            'variants': list_of_variants,
            'votes': votes_count,
            'pos_id': position.id
        }

    context = {
        'position_count': positions.count(),
        'candidate_count': candidates.count(),
        'voters_count': voters.count(),
        'positions': positions,
        'chart_data': chart_data,
        'ballot_form': ballot_form,
        'page_title': "Dashboard"
    }

    return render(request, "admin/home.html", context)

def verify(request):
    context = {
        'page_title': 'Verification'
    }
    return render(request, "voting/voter/verify.html", context)



def face_recognition_verification(user_name, timeout=3):
    print(f"Verifying {user_name}")
    """
    Recognizes if the target name appears in front of the camera within the specified timeout and saves a picture if the user is verified.

    Args:
        user_name (str): The name of the target person to recognize.
        timeout (int): Time in seconds after which the function will give up.

    Returns:
        bool: True if the target name is recognized and picture saved, False otherwise.
    """
    # Load encoding images and setup camera
    sfr = SimpleFacerec()
    sfr.load_encoding_images("/home/user/Documents/voiting_system/images")
    cap = cv2.VideoCapture(0)  # Change -1 to 0 or appropriate index based on your camera setup
    start_time = time.time()  # Record start time
    
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        
        # Detect Faces
        face_locations, face_names = sfr.detect_known_faces(frame)
        
        for face_loc, name in zip(face_locations, face_names):
            y1, x2, y2, x1 = face_loc
            # cv2.putText(frame, name, (x1, y1 - 10), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 200, 0), 2)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 200, 0), 4)

            if name.lower() == user_name.lower():
                print(f"Match found: {name}")
                # Create a directory with the user's name if it doesn't exist
                folder_path = f"/home/user/Documents/voiting_system/verified/"
                os.makedirs(folder_path, exist_ok=True)
                # Save the frame as an image file
                img_path = os.path.join(folder_path, f"{user_name}.jpg")
                cv2.imwrite(img_path, frame)
                print(f"Saved image to {img_path}")

                cap.release()
                cv2.destroyAllWindows()
                return True

        # Display the resulting frame
        cv2.imshow("Frame", frame)
        
        # Check for timeout
        if time.time() - start_time > timeout:
            print("Timeout, no match found.")
            break

        # Check for escape key
        if cv2.waitKey(1) & 0xFF == 27:  # ESC key
            break

    cap.release()
    cv2.destroyAllWindows()
    print("errorr")
    return False

def resend_otp(request):
    """API For SMS
    I used https://www.multitexter.com/ API to send SMS
    You might not want to use this or this service might not be available in your Country
    For quick and easy access, Toggle the SEND_OTP from True to False in settings.py
    """
    user = request.user
    voter = user.voter
    error = False
    if settings.SEND_OTP:
        if voter.otp_sent >= 3:
            error = True
            response = "You have requested OTP three times. You cannot do this again! Please enter previously sent OTP"
        else:
            phone = voter.phone
            # Now, check if an OTP has been generated previously for this voter
            otp = voter.otp
            if otp is None:
                # Generate new OTP
                otp = generate_otp()
                voter.otp = otp
                voter.save()
            try:
                msg = "Dear " + str(user) + ", kindly use " + \
                    str(otp) + " as your OTP"
                message_is_sent = send_sms(phone, msg)
                if message_is_sent:  # * OTP was sent successfully
                    # Update how many OTP has been sent to this voter
                    # Limited to Three so voters don't exhaust OTP balance
                    voter.otp_sent = voter.otp_sent + 1
                    voter.save()

                    response = "OTP has been sent to your phone number. Please provide it in the box provided below"
                else:
                    error = True
                    response = "OTP not sent. Please try again"
            except Exception as e:
                response = "OTP could not be sent." + str(e)

                # * Send OTP
    else:
        #! Update all Voters record and set OTP to 0000
        #! Bypass OTP verification by updating verified to 1
        #! Redirect voters to ballot page
        response = bypass_otp()
    print(otp)
    return JsonResponse({"data": response, "error": error})


def bypass_otp():
    Voter.objects.all().filter(otp=None, verified=False).update(otp="0000", verified=True)
    response = "Kindly cast your vote"
    return response


def send_sms(phone_number, msg):
    """Read More
    https://www.multitexter.com/developers
    """
    import requests
    import os
    import json
    response = ""
    email = os.environ.get('SMS_EMAIL')
    password = os.environ.get('SMS_PASSWORD')
    if email is None or password is None:
        raise Exception("Email/Password cannot be Null")
    url = "https://app.multitexter.com/v2/app/sms"
    data = {"email": email, "password": password, "message": msg,
            "sender_name": "OTP", "recipients": phone_number, "forcednd": 1}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    response = r.json()
    status = response.get('status', 0)
    if str(status) == '1':
        return True
    else:
        return False


def verify_otp(request):
    error = True
    if request.method != 'POST':
        messages.error(request, "Access Denied")
    else:
        otp = request.POST.get('otp')
        if otp is None:
            messages.error(request, "Please provide valid OTP")
        else:
            # Get User OTP
            voter = request.user.voter
            db_otp = voter.otp
            if db_otp != otp:
                messages.error(request, "Provided OTP is not valid")
            else:
                messages.success(
                    request, "You are now verified. Please cast your vote")
                voter.verified = True
                voter.save()
                error = False
    if error:
        return redirect(reverse('voterVerify'))
    return redirect(reverse('show_ballot'))


def show_ballot(request):
    # if request.user.voter.voted:
    #     messages.error(request, "You have voted already")
    #     return redirect(reverse('voterDashboard'))
    ballot = generate_ballot(display_controls=False)
    context = {
        'ballot': ballot
    }
    return render(request, "voting/voter/ballot.html", context)


def preview_vote(request):
    if request.method != 'POST':
        error = True
        response = "Please browse the system properly"
    else:
        output = ""
        form = dict(request.POST)
        # We don't need to loop over CSRF token
        form.pop('csrfmiddlewaretoken', None)
        error = False
        data = []
        positions = Ballot.objects.all()
        for position in positions:
            max_vote = position.max_vote
            pos = slugify(position.name)
            pos_id = position.id
            if position.max_vote > 1:
                this_key = pos + "[]"
                form_position = form.get(this_key)
                if form_position is None:
                    continue
                if len(form_position) > max_vote:
                    error = True
                    response = "You can only choose " + \
                        str(max_vote) + " variant for " + position.name
                else:
                    # for key, value in form.items():
                    start_tag = f"""
                       <div class='row votelist' style='padding-bottom: 2px'>
		                      	<span class='col-sm-4'><span class='pull-right'><b>{position.name} :</b></span></span>
		                      	<span class='col-sm-8'>
                                <ul style='list-style-type:none; margin-left:-40px'>
                                
                    
                    """
                    end_tag = "</ul></span></div><hr/>"
                    data = ""
                    for form_candidate_id in form_position:
                        try:
                            candidate = BallotVariant.objects.get(
                                id=form_candidate_id, ballot=position)
                            data += f"""
		                      	<li><i class="fa fa-check-square-o"></i> {candidate.name.name}</li>
                            """
                        except:
                            error = True
                            response = "Please, browse the system properly"
                    output += start_tag + data + end_tag
            else:
                this_key = pos
                form_position = form.get(this_key)
                if form_position is None:
                    continue
                # Max Vote == 1
                try:
                    form_position = form_position[0]
                    candidate = BallotVariant.objects.get(
                        Ballot=position, id=form_position)
                    output += f"""
                            <div class='row votelist' style='padding-bottom: 2px'>
		                      	<span class='col-sm-4'><span class='pull-right'><b>{position.name} :</b></span></span>
		                      	<span class='col-sm-8'><i class="fa fa-check-circle-o"></i> {candidate.name.name}</span>
		                    </div>
                      <hr/>
                    """
                except Exception as e:
                    error = True
                    response = "Please, browse the system properly"
    context = {
        'error': error,
        'list': output
    }
    return JsonResponse(context, safe=False)


def image_entropy(image_data):
    histogram = np.zeros(256, dtype=int)
    total_pixels = image_data.shape[0] * image_data.shape[1]

    for row in image_data:
        for pixel in row:
            # Manual grayscale conversion
            grayscale = int(0.299 * pixel[2] + 0.587 * pixel[1] + 0.114 * pixel[0])
            histogram[grayscale] += 1

    entropy = 0
    for count in histogram:
        if count > 0:
            probability = count / total_pixels
            entropy -= probability * np.log2(probability)

    return entropy

def submit_ballot(request):
    print("ponkilas")
    if request.method != 'POST':
        messages.error(request, "Please, browse the system properly")
        return redirect(reverse('show_ballot'))

    voter = request.user.voter
    print(voter)
    # if voter.voted_on_ballots():
    #     messages.error(request, "You have voted already")
    #     return redirect(reverse('voterDashboard'))

    form = dict(request.POST)
    form.pop('csrfmiddlewaretoken', None)
    form.pop('submit_vote', None)
    print(form)

    if len(form.keys()) < 1:
        messages.error(request, "Please select at least one candidate")
        return redirect(reverse('show_ballot'))

    # Process votes and hash the form
    form_string = str(form)
    form_hash = hashlib.sha256(form_string.encode()).hexdigest()
    username = voter.admin.first_name.lower()
    # Assuming user image path is stored in the voter model
    print("sucsses ")
    # user_image_path = f"/home/user/Documents/voiting_system/output/{username}_recognized.jpg"
    # # user_image_path = voter.user_image_path
    # entropy_value = image_entropy(user_image_path)

    # # Save entropy and hash to file
    # with open(f'/home/user/Documents/voiting_system/ballotes/{username}_vote_record.txt', 'w') as file:
    #     file.write(f"User: {username}\n")
    #     file.write(f"Image Entropy: {entropy_value}\n")
    #     file.write(f"Ballot Hash: {form_hash}\n")

    # Continue with existing voting logic
    positions = Ballot.objects.all()
    form_count = 0
    for position in positions:
        max_vote = position.max_vote
        pos = slugify(position.name)
        pos_id = position.id
        # Existing voting logic...
        
        # Ensure votes are correctly inserted
        inserted_votes = Votes.objects.filter(voter=voter)
        if inserted_votes.count() != form_count:
            inserted_votes.delete()
            messages.error(request, "Please try voting again!")
            return redirect(reverse('show_ballot'))
        else:
            voter.voted = True
            voter.save()
            messages.success(request, "Thanks for voting")
            return redirect(reverse('voterDashboard'))



def embed_voting_results_in_image(voting_data, cover_image_path, output_image_path):
    """
    Embeds voting data into a cover image and saves the result in an output image.

    :param voting_data: The voting results or ballot data to embed.
    :param cover_image_path: Path to the cover image to use for embedding.
    :param output_image_path: Path to save the image with embedded data.
    """
    # Convert voting data to a string if it's not already
    voting_data_str = str(voting_data)  # Ensure this is in a suitable format

    # Use LSB steganography to hide the voting data in the image
    secret_image = lsb.hide(cover_image_path, voting_data_str)

    # Save the image with the embedded data
    secret_image.save(output_image_path)

    print(f"Voting results embedded into {output_image_path}")



def extract_voting_results_from_image(image_path):
    """
    Extracts the embedded voting data from an image.

    :param image_path: Path to the image with embedded voting data.
    :return: The extracted voting data.
    """
    # Use LSB steganography to extract the data from the image
    hidden_data = lsb.reveal(image_path)

    return hidden_data


def generate_voting_summary(voter):
    """
    Generate a summary of the votes for a given voter.
    This function needs to be tailored to your application's data structure.
    """
    summary = "Voting Summary:\n"
    # Example: Fetch the voter's votes and compile a summary
    votes = Votes.objects.filter(voter=voter)
    for vote in votes:
        summary += f"{vote.position.name}: {vote.candidate.fullname}\n"
    return summary
