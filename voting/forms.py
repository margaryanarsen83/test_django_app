from django import forms
from .models import *
from account.forms import FormSettings


class VoterForm(FormSettings):
    class Meta:
        model = Voter
        fields = ['phone']


class PositionForm(FormSettings):
    class Meta:
        model = Ballot
        fields = ['name', 'max_vote', "priority","description"]


class CandidateForm(FormSettings):
    class Meta:
        model = BallotVariant
        # content_type = 
        fields = ['ballot', 'object_id']
        
class TextFieldOptionForm(forms.ModelForm):
    class Meta:
        model = TextField
        fields = ['name']

class CandidateForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = ['name', 'bio', 'photo']
