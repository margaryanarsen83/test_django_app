from django.db import models
from account.models import CustomUser
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

class IsVoted(models.Model):
    voted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.voted)


class Position(models.Model):
    name = models.CharField(max_length=50, unique=True)
    max_vote = models.IntegerField()
    priority = models.IntegerField()

    def __str__(self):
        return self.name




class Ballot(models.Model):
    VARIANT_TYPE_CHOICES = (
        ('candidate', 'Candidate'),
        ('text_field', 'Text Field'),
    )

    description = models.TextField()
    name = models.CharField(max_length=50, unique=True)
    max_vote = models.IntegerField()
    priority = models.IntegerField()
    variant_type = models.CharField(max_length=20, choices=VARIANT_TYPE_CHOICES)

    def __str__(self):
        return self.name
    
class Candidate(models.Model):
    name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to="candidates")
    bio = models.TextField()

    def __str__(self):
        return self.name



class TextField(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class BallotVariant(models.Model):
    ballot = models.ForeignKey(Ballot, related_name='variants', on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return f"{self.ballot.name} - {self.content_object.name}"


class Voter(models.Model):
    admin = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    phone = models.CharField(max_length=11, unique=True)
    otp = models.CharField(max_length=10, null=True)
    verified = models.BooleanField(default=False)
    voted_on_ballots = models.ManyToManyField(Ballot, through='VoteRecord', related_name='voters')
    otp_sent = models.IntegerField(default=0)
    image = models.ImageField(upload_to='user_images/', null=True, blank=True)

    def __str__(self):
        return self.admin.last_name + ", " + self.admin.first_name


class VoteRecord(models.Model):
    voter = models.ForeignKey(Voter, on_delete=models.CASCADE)
    ballot = models.ForeignKey(Ballot, on_delete=models.CASCADE)
    voted = models.BooleanField(default=False)
    vote_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.voter} voted on {self.ballot} - {self.voted}"


class Votes(models.Model):
    voter = models.ForeignKey(Voter, on_delete=models.CASCADE)
    position = models.ForeignKey(Ballot, on_delete=models.CASCADE)
    candidate = models.ForeignKey(BallotVariant, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.voter} voted for {self.candidate} in {self.position}"
