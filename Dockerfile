FROM ubuntu:20.04
# FROM 
WORKDIR ./
ADD . ./
# RUN apt-get update
# RUN apt-get -y upgrade
RUN apt-get update && \
    apt-get -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata && \
    ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    apt-get install -y libmysqlclient-dev pkg-config python 
# RUN apt-get install -y libmysqlclient-dev
# RUN apt install -y pkg-config
RUN apt-get install -y libpangocairo-1.0-0
RUN apt-get install -y python3-pip
RUN apt-get update && apt-get install -y cmake
RUN apt-get install libgtk-3-dev

RUN  apt-get install libboost-all-dev
# RUN apt-get install libglib2.0-0
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
# Collect static files
RUN python3 manage.py collectstatic --noinput

EXPOSE 8000

# Define environment variable for database password (consider using Docker secrets for production)
# ENV MYSQL_ROOT_PASSWORD=123

# Command to run the Django application
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]



# /home/user/Documents/voiting_system/e-voting-with-django/Dockerfile
# docker run --detach --name=mysql-docker-local --network my-network --env="MYSQL_ROOT_PASSWORD=123" --publish 6603:3306 --volume=/storage/docker/mysql-data:/var/lib/mysql mysql

# Use an official Python runtime as a parent image
# FROM python:3.8-slim-buster

# # Set environment variable to ensure Python output is set straight to the terminal without buffering
# # ENV PYTHONUNBUFFERED 1

# # Set working directory inside the container
# WORKDIR /app

# # Copy the current directory contents into the container at /app
# COPY . /app

# # Update package lists, install system dependencies, and clean up apt cache to reduce image size
# RUN apt-get update && \
#     apt-get -y upgrade && \
#     DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata && \
#     ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
#     dpkg-reconfigure --frontend noninteractive tzdata && \
#     apt-get install -y libmysqlclient-dev pkg-config

# # Install any needed packages specified in requirements.txt
# RUN pip install --no-cache-dir -r requirements.txt

# # Make port 8000 available to the world outside this container
# EXPOSE 8000

# # Define environment variable for database password (consider using Docker secrets for production)
# # ENV MYSQL_ROOT_PASSWORD=123

# # Command to run the Django application
# CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]




