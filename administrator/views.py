from django.shortcuts import render, reverse, redirect, get_object_or_404
from voting.models import Voter, Position, Candidate, Votes, Ballot
from account.models import CustomUser
from account.forms import CustomUserForm
from voting.forms import *
from django.contrib import messages
from django.http import JsonResponse, HttpResponse
from django.conf import settings
import json  # Not used
from django_renderpdf.views import PDFView


def find_n_winners(data, n):
    """Read More
    https://www.geeksforgeeks.org/python-program-to-find-n-largest-elements-from-a-list/
    """
    final_list = []
    candidate_data = data[:]
    # print("Candidate = ", str(candidate_data))
    for i in range(0, n):
        max1 = 0
        if len(candidate_data) == 0:
            continue
        this_winner = max(candidate_data, key=lambda x: x['votes'])
        # TODO: Check if None
        this = this_winner['name'] + \
            " with " + str(this_winner['votes']) + " votes"
        final_list.append(this)
        candidate_data.remove(this_winner)
    return ", &nbsp;".join(final_list)


class PrintView(PDFView):
    template_name = 'admin/print.html'
    prompt_download = True

    @property
    def download_name(self):
        return "result.pdf"

    def get_context_data(self, *args, **kwargs):
        title = "E-voting"
        try:
            file = open(settings.ELECTION_TITLE_PATH, 'r')
            title = file.read()
        except:
            pass
        context = super().get_context_data(*args, **kwargs)
        position_data = {}
        for position in Ballot.objects.all():
            candidate_data = []
            winner = ""
            for candidate in BallotVariant.objects.filter(ballot=position):
                this_candidate_data = {}
                votes = Votes.objects.filter(candidate=candidate, position=position).count()
                this_candidate_data['name'] = candidate.name
                this_candidate_data['votes'] = votes
                candidate_data.append(this_candidate_data)
            print("Candidate Data For  ", str(
                position.name), " = ", str(candidate_data))
            # ! Check Winner
            if len(candidate_data) < 1:
                winner = "Position does not have candidates"
            else:
                # Check if max_vote is more than 1
                if position.max_vote > 1:
                    winner = find_n_winners(candidate_data, position.max_vote)
                else:

                    winner = max(candidate_data, key=lambda x: x['votes'])
                    if winner['votes'] == 0:
                        winner = "No one voted for this yet position, yet."
                    else:
                        """
                        https://stackoverflow.com/questions/18940540/how-can-i-count-the-occurrences-of-an-item-in-a-list-of-dictionaries
                        """
                        count = sum(1 for d in candidate_data if d.get(
                            'votes') == winner['votes'])
                        if count > 1:
                            winner = f"There are {count} candidates with {winner['votes']} votes"
                        else:
                            winner = "Winner : " + winner['name']
            print("Candidate Data For  ", str(
                position.name), " = ", str(candidate_data))
            position_data[position.name] = {
                'candidate_data': candidate_data, 'winner': winner, 'max_vote': position.max_vote}
        context['positions'] = position_data
        print(context)
        return context


def dashboard(request):
    positions = Ballot.objects.all().order_by('priority')
    candidates = Candidate.objects.all()
    voters = Voter.objects.all()
    chart_data = {}

    for position in positions:
        list_of_variants = []
        votes_count = []

        for variant in BallotVariant.objects.filter(ballot=position):
            if variant.content_object:
                variant_name = getattr(variant.content_object, 'name', 'Unknown Variant')
                list_of_variants.append(variant_name)
                votes = Votes.objects.filter(candidate=variant, position=position).count()
                votes_count.append(votes)
            else:
                print(f"Warning: BallotVariant with ID {variant.id} has no associated content_object.")

        chart_data[position.name] = {
            'variants': list_of_variants,
            'votes': votes_count,
            'pos_id': position.id
        }

    context = {
        'position_count': positions.count(),
        'candidate_count': candidates.count(),
        'voters_count': voters.count(),
        'positions': positions,
        'chart_data': chart_data,
        'page_title': "Dashboard"
    }
    return render(request, "admin/home.html", context)

# def dashboard(request):
#     positions = Ballot.objects.all().order_by('priority')
#     candidates = Candidate.objects.all()
#     voters = Voter.objects.all()
#     chart_data = {}

#     for position in positions:
#         list_of_variants = []
#         votes_count = []

#         for variant in BallotVariant.objects.filter(ballot=position):
#             # Ensure content_object is not None before accessing its attributes
#             if variant.content_object:
#                 variant_name = variant.content_object.name
#                 list_of_variants.append(variant_name)
#                 votes = Votes.objects.filter(candidate=variant, position=position).count()
#                 votes_count.append(votes)
#             else:
#                 # Handle the case where content_object is None
#                 print(f"Warning: BallotVariant with ID {variant.id} has no associated content_object.")

#         chart_data[position.name] = {
#             'variants': list_of_variants,
#             'votes': votes_count,
#             'pos_id': position.id
#         }

#     context = {
#         'position_count': positions.count(),
#         'candidate_count': candidates.count(),
#         'voters_count': voters.count(),
#         'positions': positions,
#         'chart_data': chart_data,
#         'page_title': "Dashboard"
#     }
#     return render(request, "admin/home.html", context)

def voters(request):
    voters = Voter.objects.all()
    userForm = CustomUserForm(request.POST or None)
    voterForm = VoterForm(request.POST or None)
    context = {
        'form1': userForm,
        'form2': voterForm,
        'voters': voters,
        'page_title': 'Voters List'
    }
    if request.method == 'POST':
        if userForm.is_valid() and voterForm.is_valid():
            user = userForm.save(commit=False)
            voter = voterForm.save(commit=False)
            voter.admin = user
            user.save()
            voter.save()
            messages.success(request, "New voter created")
        else:
            messages.error(request, "Form validation failed")
    return render(request, "admin/voters.html", context)

def view_voter_by_id(request):
    voter_id = request.GET.get('id', None)
    voter = Voter.objects.filter(id=voter_id)
    context = {}
    if not voter.exists():
        context['code'] = 404
    else:
        context['code'] = 200
        voter = voter[0]
        context['first_name'] = voter.admin.first_name
        context['last_name'] = voter.admin.last_name
        context['phone'] = voter.phone
        context['id'] = voter.id
        context['email'] = voter.admin.email
    return JsonResponse(context)

def view_position_by_id(request):
    pos_id = request.GET.get('id', None)
    pos = Ballot.objects.filter(id=pos_id)
    context = {}
    if not pos.exists():
        context['code'] = 404
    else:
        context['code'] = 200
        pos = pos[0]
        context['name'] = pos.name
        context['max_vote'] = pos.max_vote
        context['id'] = pos.id
    return JsonResponse(context)

def updateVoter(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
    try:
        instance = Voter.objects.get(id=request.POST.get('id'))
        user = CustomUserForm(request.POST or None, instance=instance.admin)
        voter = VoterForm(request.POST or None, instance=instance)
        user.save()
        voter.save()
        messages.success(request, "Voter's bio updated")
    except:
        messages.error(request, "Access To This Resource Denied")

    return redirect(reverse('adminViewVoters'))

def deleteVoter(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
    try:
        admin = Voter.objects.get(id=request.POST.get('id')).admin
        admin.delete()
        messages.success(request, "Voter Has Been Deleted")
    except:
        messages.error(request, "Access To This Resource Denied")

    return redirect(reverse('adminViewVoters'))
def viewPositions(request):
    positions = Ballot.objects.order_by('-priority').all()  # All ballots/positions
    position_form = PositionForm(request.POST or None)  # Form to create a new position/ballot
    textfield_form = TextFieldOptionForm(request.POST or None)  # Form for text field options
    candidate_form = CandidateForm(request.POST or None, request.FILES or None)  # Form for candidate options

    context = {
        'positions': positions,  # Pass all positions/ballots to the template
        'position_form': position_form,
        'textfield_form': textfield_form,
        'candidate_form': candidate_form,
        'page_title': "Ballot"
    }

    if request.method == 'POST':
        if position_form.is_valid():  # If the position/ballot form is valid
            position = position_form.save(commit=False)  # Create a new position/ballot but don't save it yet
            position.priority = positions.count() + 1  # Assign a priority
            position.save()  # Now save the position/ballot

            
    if request.method == 'POST':
        if position_form.is_valid():  # Creating a new Ballot (Position)
            position = position_form.save(commit=False)
            position.priority = positions.count() + 1
            position.save()

            # Handling TextFieldOption
            textfields = request.POST.getlist('textfields[]')
            textfield_content_type = ContentType.objects.get_for_model(TextField)
            for text in textfields:
                if text.strip():  # Ensure it's not empty
                    textfield_option = TextField.objects.create(name=text)
                    BallotVariant.objects.create(
                        ballot=position,
                        content_type=textfield_content_type,
                        object_id=textfield_option.id,
                        content_object=textfield_option
                    )

            # Handling Candidate options
            candidate_names = request.POST.getlist('candidate_names[]')
            candidate_bios = request.POST.getlist('candidate_bios[]')
            candidate_images = request.FILES.getlist('candidate_images[]')
            candidate_content_type = ContentType.objects.get_for_model(Candidate)
            
            for i in range(len(candidate_names)):
                if candidate_names[i].strip():  # Ensure the name is not empty
                    candidate = Candidate.objects.create(
                        name=candidate_names[i],
                        bio=candidate_bios[i],
                        image=candidate_images[i]
                    )
                    BallotVariant.objects.create(
                        ballot=position,
                        content_type=candidate_content_type,
                        object_id=candidate.id,
                        content_object=candidate
                    )

            messages.success(request, "New Position and Options Created")
        else:
            messages.error(request, "Form errors")

    return render(request, "admin/positions.html", context)

# def viewPositions(request):
#     positions = Ballot.objects.order_by('-priority').all()
#     form = PositionForm(request.POST or None)
#     context = {
#         'positions': positions,
#         'form1': form,
#         'page_title': "Ballot"
#     }
#     if request.method == 'POST':
#         if form.is_valid():
#             form = form.save(commit=False)
#             form.priority = positions.count() + 1  # Just in case it is empty.
#             form.save()
#             messages.success(request, "New Position Created")
#         else:
#             messages.error(request, "Form errors")
#     return render(request, "admin/positions.html", context)
def updatePosition(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
        return redirect(reverse('viewPositions'))

    try:
        instance = Ballot.objects.get(id=request.POST.get('id'))
        pos_form = PositionForm(request.POST or None, instance=instance)

        if pos_form.is_valid():
            pos_form.save()
            messages.success(request, "Position has been updated")
        else:
            messages.error(request, "Form has errors")

    except Ballot.DoesNotExist:
        messages.error(request, "Position not found")

    return redirect(reverse('viewPositions'))

def deletePosition(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
        return redirect(reverse('viewPositions'))

    try:
        pos = Ballot.objects.get(id=request.POST.get('id'))
        pos.delete()
        messages.success(request, "Position Has Been Deleted")
    except Ballot.DoesNotExist:
        messages.error(request, "Position not found")

    return redirect(reverse('viewPositions'))

def viewCandidates(request):
    candidates = Candidate.objects.all()
    form = CandidateForm(request.POST or None, request.FILES or None)

    context = {
        'candidates': candidates,
        'form': form,
        'page_title': 'Candidates'
    }

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "New Candidate Created")
        else:
            messages.error(request, "Form errors")

    return render(request, "admin/candidates.html", context)


def updateCandidate(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
        return redirect(reverse('viewCandidates'))

    try:
        candidate = Candidate.objects.get(id=request.POST.get('id'))
        form = CandidateForm(request.POST or None, request.FILES or None, instance=candidate)

        if form.is_valid():
            form.save()
            messages.success(request, "Candidate Data Updated")
        else:
            messages.error(request, "Form has errors")

    except Candidate.DoesNotExist:
        messages.error(request, "Candidate not found")

    return redirect(reverse('viewCandidates'))




def deleteCandidate(request):
    if request.method != 'POST':
        messages.error(request, "Access Denied")
        return redirect(reverse('viewCandidates'))

    try:
        candidate = Candidate.objects.get(id=request.POST.get('id'))
        candidate.delete()
        messages.success(request, "Candidate Has Been Deleted")
    except Candidate.DoesNotExist:
        messages.error(request, "Candidate not found")

    return redirect(reverse('viewCandidates'))


def view_candidate_by_id(request):
    candidate_id = request.GET.get('id', None)
    candidate = Candidate.objects.filter(id=candidate_id)
    context = {}
    if not candidate.exists():
        context['code'] = 404
    else:
        candidate = candidate[0]
        context['code'] = 200
        context['fullname'] = candidate.fullname
        previous = CandidateForm(instance=candidate)
        context['form'] = str(previous.as_p())
    return JsonResponse(context)


def ballot_position(request):
    context = {
        'page_title': "Ballot Position"
    }
    return render(request, "admin/ballot_position.html", context)


def update_ballot_position(request, position_id, up_or_down):
    try:
        context = {
            'error': False
        }
        position = Ballot.objects.get(id=position_id)
        if up_or_down == 'up':
            priority = position.priority - 1
            if priority == 0:
                context['error'] = True
                output = "This position is already at the top"
            else:
                Ballot.objects.filter(priority=priority).update(
                    priority=(priority+1))
                position.priority = priority
                position.save()
                output = "Moved Up"
        else:
            priority = position.priority + 1
            if priority > Ballot.objects.all().count():
                output = "This position is already at the bottom"
                context['error'] = True
            else:
                Ballot.objects.filter(priority=priority).update(
                    priority=(priority-1))
                position.priority = priority
                position.save()
                output = "Moved Down"
        context['message'] = output
    except Exception as e:
        context['message'] = e

    return JsonResponse(context)


def ballot_title(request):
    from urllib.parse import urlparse
    url = urlparse(request.META['HTTP_REFERER']).path
    from django.urls import resolve
    try:
        redirect_url = resolve(url)
        title = request.POST.get('title', 'No Name')
        file = open(settings.ELECTION_TITLE_PATH, 'w')
        file.write(title)
        file.close()
        messages.success(
            request, "Election title has been changed to " + str(title))
        return redirect(url)
    except Exception as e:
        messages.error(request, e)
        return redirect("/")


def viewVotes(request):
    votes = Votes.objects.all()
    context = {
        'votes': votes,
        'page_title': 'Votes'
    }
    return render(request, "admin/votes.html", context)


def resetVote(request):
    Votes.objects.all().delete()
    Voter.objects.all().update(voted=False, verified=False, otp=None)
    messages.success(request, "All votes has been reset")
    return redirect(reverse('viewVotes'))


def create_variant(request, ballot_id):
    ballot = get_object_or_404(Ballot, id=ballot_id)
    if request.method == 'POST':
        if ballot.variant_type == 'candidate':
            candidate_id = request.POST.get('candidate_id')
            candidate = get_object_or_404(Candidate, id=candidate_id)
            content_type = ContentType.objects.get_for_model(candidate)
            BallotVariant.objects.create(
                ballot=ballot,
                content_type=content_type,
                object_id=candidate.id,
                content_object=candidate
            )
        elif ballot.variant_type == 'text_field':
            text_field_id = request.POST.get('text_field_id')
            text_field = get_object_or_404(TextField, id=text_field_id)
            content_type = ContentType.objects.get_for_model(text_field)
            BallotVariant.objects.create(
                ballot=ballot,
                content_type=content_type,
                object_id=text_field.id,
                content_object=text_field
            )
        return redirect('view_ballot', ballot_id=ballot.id)

    candidates = Candidate.objects.all() if ballot.variant_type == 'candidate' else []
    text_fields = TextField.objects.all() if ballot.variant_type == 'text_field' else []
    context = {
        'ballot': ballot,
        'candidates': candidates,
        'text_fields': text_fields,
    }
    return render(request, 'admin/create_variant.html', context)


def ballot_detail(request, ballot_id):
    ballot = get_object_or_404(Ballot, id=ballot_id)
    context = {
        'ballot': ballot,
    }
    return render(request, 'admin/ballot_detail.html', context)
