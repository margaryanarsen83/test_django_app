from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from .email_backend import EmailBackend
from django.contrib.auth import login, logout
from .forms import CustomUserForm
from voting.forms import VoterForm

def account_login(request):
    if request.user.is_authenticated:
        return redirect(reverse("adminDashboard") if request.user.user_type == '1' else reverse("voterDashboard"))

    context = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = EmailBackend.authenticate(request, username=email, password=password)
        if user:
            login(request, user)
            return redirect(reverse("adminDashboard") if user.user_type == '1' else reverse("voterDashboard"))
        else:
            messages.error(request, "Invalid login details.")
            return redirect(reverse('account_login'))
    return render(request, "voting/login.html", context)


def account_register(request):
    if request.method == 'POST':
        userForm = CustomUserForm(request.POST)
        voterForm = VoterForm(request.POST)
        if userForm.is_valid() and voterForm.is_valid():
            user = userForm.save()
            voter = voterForm.save(commit=False)
            voter.admin = user
            voter.save()
            messages.success(request, "Account created. You can now log in.")
            return redirect(reverse('account_login'))
        else:
            messages.error(request, "Provided data failed validation.")
    else:
        userForm = CustomUserForm()
        voterForm = VoterForm()

    context = {
        'form1': userForm,
        'form2': voterForm
    }
    return render(request, "voting/reg.html", context)



def account_logout(request):
    if request.user.is_authenticated:
        logout(request)
        messages.success(request, "You have been successfully logged out.")
    else:
        messages.error(request, "You need to be logged in to log out.")
    return redirect(reverse("account_login"))

